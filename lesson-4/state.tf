terraform {
  backend "s3" {
    bucket = "terraform-s3-a"
    key    = "terraform-learn/sg.tfstate"
    region = "ap-south-1"
  }
}
