variable region {
  default = "us-east-1"
}

variable ami_id {
  type = map
  default = {
    us-east-1  = "ami-0b0ea68c435eb488d" 
    ap-south-1 = "ami-0f2e255ec956ade7f"
  }
}
